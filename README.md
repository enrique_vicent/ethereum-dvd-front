Setting up the environment
======

- pre requisites: bower

```bash
bower install
```

- recomended: brackets; to serve the web.

running with docker
===============

tree options

1. see : https://medium.com/decentralized-capital/setting-up-a-parity-ethereum-node-in-docker-and-connect-safely-f881faa17686#.tvfqtlvg7

2. use: 

```bash
docker run -ti -d -p 0.0.0.0:8545:8545 --name my-parity ethcore/parity --jsonrpc-interface all --jsonrpc-hosts all
```

3. use `docker-compose up -d`

running from installation
=========

a fresh installation does not expose rpc properly. To fix it run

```bash
parity  --testnet --jsonrpc-interface '0.0.0.0' --jsonrpc-hosts all --rpccorsdomain "*" --jsonrpc-apis web3,eth,net,personal,parity,parity_set,traces,rpc,parity_accounts
```