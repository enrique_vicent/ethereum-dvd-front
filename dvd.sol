contract DVD {

   address sender;
   address acepter;
   
   struct File {
       bytes32 period;
       uint8 acepted; //0 not found //1 proposed //2 acepted
   }
   
   event evnt(
       bool success,
       bytes32 cause,
       bytes32 hash,
       bytes32 operation
   );
   
   mapping (bytes32 => File) files;
   
   // Create a new ballot with $(_numProposals) different proposals.
   function DVD (address senderAddress, address acepterAddress) {
       sender = senderAddress;
       acepter = acepterAddress;
   }
   
   function getFile (bytes32 fileHash) constant returns ( uint8 status, bytes32 period) {
     File file = files[fileHash];
     return (file.acepted, file.period);
   }
   
   function createFile (bytes32 fileHash, bytes32 period) {
     if (msg.sender != sender){
         evnt(false, 'not autorized', fileHash, 'createFile');
     } else if (files[fileHash].acepted!=0) {
         evnt(false, 'invalid state', fileHash, 'createFile');
     } else {
         File memory file;
         file.acepted=1;
         file.period=period;
         files[fileHash] = file;
         evnt(true, '', fileHash, 'createFile');
     }
   }
   
   function acceptFile (bytes32 fileHash) {
     if (msg.sender != acepter){
         evnt(false, 'not autorized', fileHash, 'acceptFile');
     } else if (files[fileHash].acepted!=1){
         evnt(false, 'invalid state', fileHash, 'acceptFile');
     } else {
         files[fileHash].acepted=2;
         evnt(true, '', fileHash, 'acceptFile');
     }
   }

}