var app = angular.module('testproject', []);

app.factory('myEthereum',['$rootScope', function($rootScope){
  var o = {
  };
  o.contract = null;
  o.contractInstance = null;
  o.Web3 = require('web3');
  o.web3 = new Web3();

  o.init = function (serverURL, contractAbi, contractAddress){
    o.web3.setProvider(new o.web3.providers.HttpProvider(serverURL));
    o.contract = o.web3.eth.contract(contractAbi);
    o.contractInstance = o.contract.at(contractAddress);
  }

  o.getFile = function(hash){
    var rawResult =  o.contractInstance.getFile(hash);
    var result = {}
    if (rawResult[0]=="1"){
      result.status='proposed';
    }
    else if (rawResult[0]=="2"){
      result.status='validated';
    }
    else {
      result.status='not found'
    }
    result.period=o.web3.toAscii(rawResult[1])
    ;
    return result;
  }

  o.createFile = function (hash, period, from){
    o.web3.personal.unlockAccount(from.address, from.lock);
    o.contractInstance.createFile(hash, o.web3.fromAscii(period, 32),{from: from.address});
  }

  o.validateFile = function (hash, from){
    o.web3.personal.unlockAccount(from.address, from.lock);
    o.contractInstance.acceptFile(hash, {from: from.address});
  }

  o.subscribe = function (onEvent){
    o.contractInstance.evnt(onEvent);
  }

  return o;
}]);

app.controller('appController', ['myEthereum', function(eth) {
  var ctx = this;
  ctx.passwrd='';
  ctx.ctes = {
    accesURL: 'http://127.0.0.1:8545',
    accounts: {
      creator: {
        address:'0x009976eFc4bA76f7D753B804566e6Af7B9Aa9909',
        lock:'hola' //idealmente este campo estaría vacio hasta que e usuario lo poroporcionara via campo passwd
      },
      aceptor: {
        address:'0x00cC648Db5C82972C39ca76665AA02bBa864829A',
        lock:'hola'
      }
    },
    contract: {
      address: '0xb49F22D3Ebe00987831CB390D3b4f1C9FA01084c',
      abi:[{"constant":false,"inputs":[{"name":"fileHash","type":"bytes32"},{"name":"period","type":"bytes32"}],"name":"createFile","outputs":[],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"fileHash","type":"bytes32"}],"name":"acceptFile","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"fileHash","type":"bytes32"}],"name":"getFile","outputs":[{"name":"status","type":"uint8"},{"name":"period","type":"bytes32"}],"payable":false,"type":"function"},{"inputs":[{"name":"senderAddress","type":"address"},{"name":"acepterAddress","type":"address"}],"payable":false,"type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"name":"success","type":"bool"},{"indexed":false,"name":"cause","type":"bytes32"},{"indexed":false,"name":"hash","type":"bytes32"},{"indexed":false,"name":"operation","type":"bytes32"}],"name":"evnt","type":"event"}]
    }
  };

  function onEvent (error, result){
    if (!error){
      console.log(result);
      window.alert(result);
    }
  }

  function init(){
    eth.init(ctx.ctes.accesURL, ctx.ctes.contract.abi, ctx.ctes.contract.address);
    eth.subscribe(onEvent);
  }
  init();

  ctx.query = function (){
    console.info('query');
    var result = eth.getFile(ctx.file.hash);
    ctx.file.status=result.status;
    ctx.file.period=result.period;
    ctx.file.statusClass=statusClass();
  }

  function statusClass(){
    switch (ctx.file.status){
      case 'proposed':
        return 'label label-info';
        break;
      case 'validated':
        return 'label label-success';
        break;
      case 'not found':
        return 'label label-danger';
        break;
      default:
        return '';
    }
  }

  ctx.create = function(){
    console.info('create');
    eth.createFile(ctx.file.hash, ctx.file.period, ctx.ctes.accounts.creator);
  }

  ctx.validate = function() {
    console.info('validate');
    eth.validateFile(ctx.file.hash, ctx.ctes.accounts.aceptor);
  }

}]);
